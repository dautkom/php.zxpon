<?php

namespace dautkom\zxpon\ont;
use dautkom\zxpon\ONT;


/**
 * TODO: Количество переданных байт
 * TODO: Управление вланом 50%
 * @property Net $Net
 * @package dautkom\zxpon\ont
 */
class Net extends ONT
{

    /**
     * Retrieve port amount
     *
     * @return int
     */
    public function getPortCount(): int
    {
        return count(self::$device['ports']);
    }

    /**
     * Retrieve ONT LAN ports states
     * array (
     *     'state' => int,
     *     'link'  => int,
     * )
     *
     * State values:
     *   unlock (1)
     *   lock (2)
     *
     * Link values:
     *   auto     (1)
     *   half10   (2)
     *   full10   (3)
     *   half100  (4)
     *   full100  (5)
     *   full1000 (6)
     *
     * @return array
     */
    public function getLinkState()
    {

        $link       = [];
        $port_count = $this->getPortCount();

        for( $i=0; $i < $port_count; $i++ )
        {

            // GET ETH port state: enable/disable
            $oid               = ".1.3.6.1.4.1.3902.1012.3.50.14.1.1.5." . parent::$instance_olt . '.' . parent::$port['ont'] . '.' . ($i+1);
            $link[$i]['state'] = $this->get($oid);

            // GET ETH port link status
            $oid              = ".1.3.6.1.4.1.3902.1012.3.50.14.1.1.7." . parent::$instance_olt . '.' . parent::$port['ont'] . '.' . ($i+1);
            $link[$i]['link'] = $this->get($oid);

        }

        return $link;

    }


    /**
     * Get VLAN port - [portid]([mode]|[access]|[trunk])
     *
     * @return array
     */
    public function getVlanPort()
    {

        $vlanport   = [];
        $port_count = $this->getPortCount();

        for( $i=0; $i < $port_count; $i++ )
        {
            // Empty array
            $vlanport[$i]['mode']   = '';
            $vlanport[$i]['access'] = '';
            $vlanport[$i]['trunk']  = '';

            // Get VLAN PORT Mode: access (1) / trunk (2) / unknown(5)
            $oid                  = ".1.3.6.1.4.1.3902.1012.3.50.15.100.1.1.3." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1.' . ($i+1);
            $vlanport[$i]['mode'] = $this->get($oid);

            // if port mode access -> PVID, trunk-> trunk vlan list
            switch ($vlanport[$i]['mode'])
            {

                case '1':
                    $oid                    = ".1.3.6.1.4.1.3902.1012.3.50.15.100.1.1.4." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1.' . ($i+1);
                    $vlanport[$i]['access'] = $this->get($oid);
                break;

                case '2':
                    $oid                   = ".1.3.6.1.4.1.3902.1012.3.50.15.100.1.1.7." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1.' . ($i+1);
                    $vlanport[$i]['trunk'] = $this->get($oid);
                break;

            }

        }

        return $vlanport;

    }


    /**
     * Get Customer ONT Tarif
     *
     * @throw \RuntimeException
     * @return bool|int|string  Tarif name or false
     */
    public function getTarif()
    {

        // Traffic rates:
        $tarif_params = [];

        // Get information about ONT tarif
        $tcont           = $this->get(".1.3.6.1.4.1.3902.1012.3.30.1.1.3." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1');
        $gemport['up']   = $this->get(".1.3.6.1.4.1.3902.1012.3.30.2.1.6." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1');
        $gemport['down'] = $this->get(".1.3.6.1.4.1.3902.1012.3.30.2.1.7." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1');

        // Check params
        if( $tcont === false || $gemport['up'] === false || $gemport['up'] === false )
        {
            throw new \RuntimeException('ONT does not have configured tarif');
        }

        // Get traffic profile and tconts tables
        $traffic_profiles = $this->getTraficProfileTable();
        $tcont_profiles   = $this->getTcontProfileTable();

        if( !array_key_exists($tcont, $tcont_profiles) || !array_key_exists($gemport['up'], $traffic_profiles) || !array_key_exists($gemport['down'], $traffic_profiles) )
        {
            throw new \RuntimeException('There is no information in traffic profile table about tcont or gemport snmp instance oid');
        }

        $tarif_params['tcont']         = $tcont_profiles[$tcont];
        $tarif_params['gemport_up']    = $traffic_profiles[$gemport['up']];
        $tarif_params['gemport_down']  = $traffic_profiles[$gemport['down']];

        return $tarif_params;

    }


    /**
     * Установка тарифного плана
     *
     * @param string $tcont Tcont profile name
     * @param string $gemport_up Gemport upstream profile name
     * @param string $gemport_down Gemport downstream profile name
     * @throw \RuntimeException
     * @return bool
     */
	public function setTarif($tcont, $gemport_up, $gemport_down)
    {

        // Get traffic profile and tconts tables
        $traffic_profiles = $this->getTraficProfileTable();
        $tcont_profiles   = $this->getTcontProfileTable();

        // Check profile names in traffic profile table
        $tcont_oid = array_search($tcont, $tcont_profiles);
        if($tcont_oid === false)
        {
            throw new \RuntimeException("Tcont name $tcont does not exist in tcont traffic table");
        }

        $gemport_up_oid   = array_search($gemport_up, $traffic_profiles);
        $gemport_down_oid = array_search($gemport_down, $traffic_profiles);

        if( $gemport_up_oid === false || $gemport_down_oid === false )
        {
            throw new \RuntimeException('Gemport does not exist in traffic profile table');
        }

        $ret = parent::$snmp->set(
            [
                ".1.3.6.1.4.1.3902.1012.3.30.2.1.6." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1',
                ".1.3.6.1.4.1.3902.1012.3.30.2.1.7." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1',
                ".1.3.6.1.4.1.3902.1012.3.30.1.1.3." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1'
            ],
            ['i', 'i', 'i'],
            [
                $gemport_up_oid,
                $gemport_down_oid,
                $tcont_oid
            ]
        );

		return $ret;

	}


	/**
     * ONT Gemport state
     * @return bool Enabled->true, disabled->false
     */
    public function getGemPortState()
    {

        //.1 - Порядковый номер Gem порта
        $oid = ".1.3.6.1.4.1.3902.1012.3.30.2.1.11." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1';
        $gps = $this->get($oid);

        return $gps == '1' ? true : false;

    }


    /**
     * Изменение статуса GemPort(статус интернета)
     *
     * @param  bool $state
     * @return bool
     */
	public function setGemPortState($state)
    {

        // Type matching
        $state = intval(!boolval($state))+1;

		// По умолчанию интернет на первом GemPort'e
		return parent::$snmp->set(".1.3.6.1.4.1.3902.1012.3.30.2.1.11." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1', 'i', $state);

	}


	/**
     * Мак адрес можно получить по следующему OID 1.3.6.1.4.1.3902.1015.6.1.3.1.1.1073741824.509.136.174.29.127.38.15 где:
     * 1.3.6.1.4.1.3902.1015.6.1.3.1.1. - общая ветка
     * .1073741824. - идентификатор ONT (parent::$instance_onu), хрен знает как определить под каким идентификатором терминал. 1073741824 - 1/1/1/1, а следующий 1073741824+256=1073742080 -> 1/1/1/2
     * .509. - Vlan ID
     * .136.174.29.127.38.15. - Mac address
     *
     * @return mixed False if no snmp data. Array[id][mac]|[vlan]
     */
    public function getFDBMacAddress()
    {

        $instance   = parent::$instance_onu;
        $oid        = ".1.3.6.1.4.1.3902.1015.6.1.3.1.5.1.$instance";
        $result     = @$this->walk($oid, true);

        if($result) {

            $i          = 0;
            $macaddress = false;

            foreach ($result as $key => $value) {

                $mac = $key;
                $mac = preg_replace("/.*$instance/", "", $mac);

                // Check returned data
                $pattern = '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/';

                // Не всегда snmpwalk возвращает верные значения.
                if (preg_match($pattern, $mac)) {

                    list($vlan, $oct1, $oct2, $oct3, $oct4, $oct5, $oct6) = explode(".", $mac);

                    $macaddress[$i]['mac']  = sprintf("%02s:%02s:%02s:%02s:%02s:%02s", dechex($oct1),dechex($oct2),dechex($oct3),dechex($oct4),dechex($oct5),dechex($oct6));
                    $macaddress[$i]['vlan'] = $vlan;

                    $i++;

                }
                else{
                    echo 'Please, refresh page';
                }
            }

            return $macaddress;

        }

        return false;

    }


    /**
     * Получение списка всех вланов на OLT
     *
     * @return array Vlan list[vid]=name
     */
    public function getVlanList()
    {

        // Get VLAN IDs
        $vlan  = [];
        $oid   = ".1.3.6.1.4.1.3902.3.102.1.1.1.1";
        $vlans = $this->walk($oid, false);

        // Get VLAN names
        foreach ($vlans as $vid) {
            $vlan[$vid] = $this->get(".1.3.6.1.4.1.3902.3.102.1.1.1.6.$vid");
        }

        return $vlan;

    }


    /**
     * Установка влана на порту. На данный момент нет возможности изменить список тагированных портов.
     *
     * @param  int    $port    номер порта
     * @param  string $mode    Режим порта: access или trunk
     * @param  int    $vid     Идентификатор влана
     * @return bool
     */
	public function setVlanOnPort($port, $mode, $vid)
	{

        // Только порт в режиме access
		if($mode != 'access') {
            return false;
        }

		return parent::$snmp->set(".1.3.6.1.4.1.3902.1012.3.50.15.100.1.1.4." . parent::$instance_olt . '.' . parent::$port['ont'] . ".1.$port", 'i', $vid);

	}


    /**
     * Retrieve QoS information
     *
     * @param  int $vport  Vport ID
     * @return int|bool
     */
    public function getQos($vport = 1)
    {

        $qos = $this->get("1.3.6.1.4.1.3902.1015.21.4.4.1.1.5.1." . implode('.', parent::$port) . ".4.$vport");

        if (!preg_match('/^[0-7]$/', $qos))
        {
            trigger_error('Retrieved invalid QoS number', E_USER_WARNING);
        }

        $qos = intval($qos);

        // Check QoS mark mode override setting.
        // trust (1); override (2)
        $markmode = $this->get("1.3.6.1.4.1.3902.1015.21.4.4.1.1.2.1." . implode('.', parent::$port) . ".4.$vport");
        $markmode = intval($markmode);

        // if qos=0 markmode must be 1
        // or if [1 ; qos ; 7] markmode must be 2
        if ( ($qos === 0 && $markmode === 1) || ($qos >= 1 && $qos <= 7 && $markmode === 2) )
        {
            return $qos;
        }
        else {
            trigger_error('Invalid QoS mark mode setting', E_USER_NOTICE);
            return $qos;
        }

    }


    /**
     * Set QoS information
     *
     * @param int $qos      QoS number (0..7)
     * @param int $vport    Vport ID
     * @throw \RuntimeException
     * @return bool
     */
    public function setQos($qos, $vport = 1)
    {

        // Validate input qos number
        if (!preg_match('/^[0-7]$/', $qos))
        {
            throw new \RuntimeException('Invalid QoS number. Valid numbers: 0..7');
        }

        // Validate vport number
        if (!preg_match('/^[1-9]$/', $vport))
        {
            throw new \RuntimeException('Invalid gemport number');
        }

        // Mark mode: trust (1); override (2)
        $markmode = ($qos >= 1 && $qos <= 7) ? 2 : 1;

        $result = parent::$snmp->set(
            [
                "1.3.6.1.4.1.3902.1015.21.4.4.1.1.5.1." . implode('.', parent::$port) . ".4.$vport",
                "1.3.6.1.4.1.3902.1015.21.4.4.1.1.2.1." . implode('.', parent::$port) . ".4.$vport",
            ],
            ['i', 'i'],
            [
                $qos,
                $markmode
            ]
        );

        return $result;

    }


    /**
     * Retrieve ServicePort state.
     * enabled  (1)
     * disabled (2)
     *
     * @param int $serviceport_id
     * @return int ServicePort state
     */
    public function getServicePortState( $serviceport_id = 1 )
    {

        // validate input data. Service port ID range: 1-9
        if (!preg_match('/^[1-9]$/', $serviceport_id))
        {
            throw new \RuntimeException('Invalid service port id');
        }

        $lastpart_oid = strval(intval(parent::$instance_onu) + $serviceport_id - 1);
        return $this->get("1.3.6.1.4.1.3902.1015.8.1.1.1.22.$lastpart_oid.$serviceport_id");

    }


    /**
     * Set ServicePort state
     * enabled  (1)
     * disabled (2)
     *
     * @param int $state            State param
     * @param int $serviceport_id   ServicePort ID
     * @throw \RuntimeException
     * @return bool|null
     */
    public function setServicePortState( $state, $serviceport_id = 1 )
    {

        // Type matching
        $state = intval($state);

        if( !($state === 1 || $state === 2) )
        {
            throw new \RuntimeException('Wrong service port state params');
        }

        if (!preg_match('/^[1-9]$/', $serviceport_id))
        {
            throw new \RuntimeException('Invalid service port id');
        }

        $lastpart_oid = strval(intval(parent::$instance_onu) + $serviceport_id - 1);
        return parent::$snmp->set("1.3.6.1.4.1.3902.1015.8.1.1.1.22.$lastpart_oid.$serviceport_id", 'i', $state);

    }


    /**
     * Retrieve full traffic profile table.
     * Array form:
     * <code>
     * $array = array(
     *   '1879048195' => '15M',
     *   '1879048196' => '100M'
     * );
     * </code>
     * @return array|bool
     */
    private function getTraficProfileTable()
    {
        return $this->walk("1.3.6.1.4.1.3902.1012.3.26.2.1.2", true);
    }


    /**
     * Retrieve full tcont traffic profile table.
     * Array form:
     * <code>
     * $array = array(
     *      '1879048195' => '50M-UP',
     *      '1879048196' => '100M-UP'
     * );
     * </code>
     * @return array|bool
     */
    private function getTcontProfileTable()
    {
        return $this->walk("1.3.6.1.4.1.3902.1012.3.26.1.1.2", true);
    }

}
