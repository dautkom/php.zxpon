<?php

namespace dautkom\zxpon\ont\ZTEF625G;
use Exception;
use dautkom\zxpon\ont\{
    Net, Phone, TV
};


/**
 * @property Net $Net
 * @property Phone $Phone
 * @property TV $TV
 * @package dautkom\zxpon\ont\ZTEF625G
 */
class Common extends \dautkom\zxpon\ont\Common
{

    /** @noinspection PhpMissingParentConstructorInspection
     *  @ignore
     *  @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->Net   = new Net();
        $this->Phone = new Phone();
        $this->TV    = new TV();
    }

}
