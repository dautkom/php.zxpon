<?php

namespace dautkom\zxpon\ont;
use dautkom\zxpon\ONT;


/**
 * @property Net $Net
 * @property Phone $Phone
 * @property TV $TV
 * @property WiFi $WiFi
 * @package dautkom\zxpon\ont
 */
class Common extends ONT
{

    /**
     * Common constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        parent::loadIni();
    }

    /**
     * Return an array of supported services
     *
     * @return array
     */
    public function getServices()
    {
        return array_keys(get_object_vars($this));
    }


    /**
     * @return array
     */
    public function getPort()
    {
       return parent::$port;
    }


    /**
     * Retriave Serial Number of ONT
     * Вид серийного номера ZTEGC10201B6
     * SNMP возврашает серийный в виде HEХ
     *
     * @return string
     */
    public function getSN()
    {

        $sn     = $this->get(".1.3.6.1.4.1.3902.1012.3.28.1.1.5." . parent::$instance_olt . '.' . parent::$port['ont']);
        $sn     = explode(" ", $sn);
        $return = '';

        foreach ($sn as $key => $value)
        {
            if ($key < 4) {
                $return.=chr(hexdec($value));   // Первые 4 символа переводятся из HEX->DEC, и подставляется в ASCII таблицу
            }
            else{
                $return.=$value;                // Остальные символы неизменны
            }
        }

        return $return;

    }


    /**
     * Retrieve model name
     * @return string
     */
    public function getModelName()
    {
        return parent::$model;
    }


    /**
     * Retrieves model directly from ONT
     *
     * @return string
     */
    public function getOntModel()
    {
        return $this->get(".1.3.6.1.4.1.3902.1012.3.50.11.2.1.17." . parent::$instance_olt . '.' . parent::$port['ont']);
    }


    /**
     * Get admin status
     * enable (1)
     * disable (2)
     * @return int
     */
    public function getRegStatus()
    {
        return $this->get(".1.3.6.1.4.1.3902.1012.3.28.2.1.1." . parent::$instance_olt . '.' . parent::$port['ont']);
    }


	/**
	 * Set ONT registration Status
	 * !!!После включения терминала - дальнейшия регисрация происходит на протяжении 30 мин или более
     *
	 * @param  bool $status 1-enabled, 2-disabled
	 * @return null|bool Результат записи.
	 */
	public function setRegStatus($status)
    {

        // Type matching
        $status = intval($status);

        if( $status < 1 || $status > 2)
        {
            trigger_error('Wrong registration status params', E_USER_WARNING);
            return null;
        }

		return parent::$snmp->set(".1.3.6.1.4.1.3902.1012.3.28.2.1.1." . parent::$instance_olt . '.' . parent::$port['ont'],'i', $status);

	}


	/**
     * Retrieve ONT phase state
     * logging (0)
     * los (1)
     * syncMib (2)
     * working (3)
     * dyinggasp (4)
     * authFailed (5)
     * offline (6)
     *
     * @return int
     */
    public function getRegPhaseState()
    {
        return $this->get(".1.3.6.1.4.1.3902.1012.3.28.2.1.4." . parent::$instance_olt . '.' . parent::$port['ont']);
    }


    /**
     * Retrieve ONT 0107 state
     * unknown (0)
     * o1-initial (1)
     * o2-standby (2)
     * o3-powersetup (3)
     * o4-serialnumber (4)
     * o5-ranging (5)
     * o6-operation (6)
     * o7-popup (7)
     * @return int
     */
    public function getRegO1O7State()
    {
        return $status['OntO1O7State'] = $this->get(".1.3.6.1.4.1.3902.1012.3.28.2.1.3." . parent::$instance_olt . '.' . parent::$port['ont']);
    }


    /**
     * Получение уровней сигнала от ONT
     *
     * @return array
     */
    public function getOpticalLevel()
    {

        // If ONT not registered
        if(!$this->getOMCCStatus())
        {
            return [
                'rx' => null,
                'tx' => null
            ];
        }

        // Если оптический уровень равен 0 db то snmp значение будет равно 15000;
        $startvalue = 15000;

        $rx  = ".1.3.6.1.4.1.3902.1012.3.50.12.1.1.10." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1';
        $tx  = ".1.3.6.1.4.1.3902.1012.3.50.12.1.1.14." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1';
        $raw = $this->get([$rx, $tx]);

        return [
            'rx' => ( intval($raw[$rx]) - $startvalue) * 0.002 ,
            'tx' => ( intval($raw[$tx]) - $startvalue) * 0.002 ,
        ];

    }


    /**
     * ONT статус регистрации
     *
     * @return bool
     */
    public function getOMCCStatus()
    {
        // Retrieve OMCC status info. ONT registered = true(1), ONT unregistered = false(2);
        $status = @$this->get("1.3.6.1.4.1.3902.1012.3.28.2.1.2." . parent::$instance_olt . '.' . parent::$port['ont']);
        return $status == 1 ? true : false;
    }

}
