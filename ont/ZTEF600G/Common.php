<?php

namespace dautkom\zxpon\ont\ZTEF600G;
use dautkom\zxpon\ont\Net;


/**
 * @property Net $Net
 * @package dautkom\zxpon\ont\ZTEF600G
 */
class Common extends \dautkom\zxpon\ont\Common
{

    /** @noinspection PhpMissingParentConstructorInspection
     *  @ignore
     *  @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->Net = new Net();
    }

}
