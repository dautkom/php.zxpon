<?php

namespace dautkom\zxpon\ont;
use dautkom\zxpon\ONT;


/**
 * @property Phone $Phone
 * @package dautkom\zxpon\ont
 */
class Phone extends ONT
{

    /**
     * Pot port count
     * @var int
     */
    private static $pot_port_max = 2;

    /**
     * @var bool
     */
    private static $voip_configured = false;


    /**
     * Retrive VoIP Pot port state
     * unlock (1)
     * lock (2)
     *
     * @return array
     */
    public function getVoIPState()
    {

        $state = [];
        
        for($i = 0; $i < self::$pot_port_max; $i++) {
            $oid       = ".1.3.6.1.4.1.3902.1012.3.50.17.1.1.1." . parent::$instance_olt . '.' . parent::$port['ont'] . '.' . ($i+1);
            $state[$i] = $this->get($oid);
        }

        return $state;

    }


    /**
     * @return bool
     */
    public function isVoIPDataExists()
    {

        $oid        = ".1.3.6.1.4.1.3902.1012.3.50.17.8.1.3." . parent::$instance_olt . '.' . parent::$port['ont'];
        $configured = @$this->walk($oid);

        self::$voip_configured = $configured ? true : false;
        return self::$voip_configured;

    }


    /**
     * @return array
     */
    public function getVoIPRegStatus()
    {

        $result = [];

        for ($i = 0; $i < self::$pot_port_max; $i++) {
            $oid        = ".1.3.6.1.4.1.3902.1012.3.50.17.6.1.2." . parent::$instance_olt . '.' . parent::$port['ont'] . '.' . ($i+1);
            $result[$i] = $this->get($oid);
        }

        return $result;

    }


    /**
     * @return array
     */
    public function getVoIPHookStatus()
    {

        $result = [];
               
        for ($i = 0; $i < self::$pot_port_max; $i++) {
            $oid        = ".1.3.6.1.4.1.3902.1012.3.50.17.1.1.10." . parent::$instance_olt . '.' . parent::$port['ont'] . '.' . ($i+1);
            $result[$i] = $this->get($oid);
        }

        return $result;

    }

}
