<?php

namespace dautkom\zxpon\ont\ZTEF668;
use Exception;
use dautkom\zxpon\ont\{
    Net, Phone, TV, WiFi
};

/**
 * @property Net $Net
 * @property TV $TV
 * @property Phone $Phone
 * @property WiFi $WiFi
 * @package dautkom\zxpon\ont\ZTEF668
 */
class Common extends \dautkom\zxpon\ont\Common
{

    /** @noinspection PhpMissingParentConstructorInspection
     * @throws Exception
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();

        $this->Net   = new Net();
        $this->Phone = new Phone();
        $this->TV    = new TV();
        $this->WiFi  = new WiFi();
    }

}
