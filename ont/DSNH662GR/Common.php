<?php

namespace dautkom\zxpon\ont\DSNH662GR;
use dautkom\zxpon\ont\{
    Net, TV
};


/**
 * @property Net $Net
 * @package dautkom\zxpon\ont\DSNH662GR
 */
class Common extends \dautkom\zxpon\ont\Common
{

    /** @noinspection PhpMissingParentConstructorInspection
     *  @ignore
     *  @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->Net = new Net();
        $this->TV  = new TV();
    }

    /**
     * Retrieves model directly from ONT
     *
     * @return string
     */
    public function getOntModel()
    {
        return null;
    }

}
