<?php

namespace dautkom\zxpon\ont\ZTEF660;
use dautkom\zxpon\ont\{
    Net, Phone, WiFi
};


/**
 * @property Net $Net
 * @property Phone $Phone
 * @property WiFi $WiFi
 * @package dautkom\zxpon\ont\ZTEF660
 */
class Common extends \dautkom\zxpon\ont\Common
{

    /** @noinspection PhpMissingParentConstructorInspection
     *  @ignore
     *  @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->Net   = new Net();
        $this->Phone = new Phone();
        $this->WiFi  = new WiFi();
    }

}
