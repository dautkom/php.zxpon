<?php

namespace dautkom\zxpon\ont;
use dautkom\zxpon\ONT;


/**
 * @property TV $TV
 * @package dautkom\zxpon\ont
 */
class TV extends ONT
{

    /**
     * Get TV interface admin state status
     *
     * @return bool If true -> enabled, false-> disabled
     */
    public function getAdminState()
    {

        $oid   = ".1.3.6.1.4.1.3902.1012.3.50.19.1.1.1." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1';
        $state = $this->get($oid);

        return $state == '1' ? true : false;

    }

    /**
     * Change TV interface admin state (true/false)
     *
     * @param  bool|int $state
     * @return bool
     */
    public function setAdminState($state)
    {

        $status = intval(!boolval($state))+1;
        $oid    = ".1.3.6.1.4.1.3902.1012.3.50.19.1.1.1." . parent::$instance_olt . '.' . parent::$port['ont'] . '.1';

        return parent::$snmp->set($oid, 'i', $status);

    }

} 