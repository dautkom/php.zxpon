<?php

namespace dautkom\zxpon;
use dautkom\netsnmp;


/**
 * Interface iOLT for warning mitigation:
 * Strict Standards: Declaration of ONT::connect() should be compatible with that of OLT::connect()
 * @package dautkom\zxpon
 */
interface iOLT
{
    /**
     * @param  $arg1
     * @param  $arg2
     * @return mixed
     */
    public function connect($arg1, $arg2);
}


/**
 * @package dautkom\zxpon
 */
class OLT extends Core implements iOLT
{

    /**
     * @param  string $ip
     * @param  array  $snmp
     * @return OLT
     */
    public function connect($ip, $snmp=[])
    {

        parent::$snmp = (new netsnmp\NetSNMP())->init($ip, $snmp);
        parent::$ip   = $this->filterIp($ip);
        parent::$port = ['rack', 'card', 'olt', 'ont'];

        return $this;

    }


    /**
     * @return ONT
     */
    public function ONT()
    {
        return new ONT();
    }


    /**
     * @param  string $ip
     * @return string
     * @throws \RuntimeException
     */
    private function filterIp(string $ip): string
    {

        $ip = trim($ip);

        if(empty($ip)) {
            $ip = self::$ip;
        }

        if( !empty($ip) && !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            throw new \RuntimeException( 'Wrong IP-address specified' );
        }

        return $ip;

    }

}
