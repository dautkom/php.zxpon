# ZTE GPON Library

PHP classes library for ZTE GPON ONT and OLT managment.

## Requirements

* PHP 7.0 or higher
* php_snmp extension
* dautkom/netsnmp package

## Supported models

* F600G
* F625G
* F660
* F668

## License

Copyright (c) 2013-2016 Jevgenijs Kalinins and respective contributors.
