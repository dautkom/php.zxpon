<?php

namespace dautkom\zxpon;
use dautkom\netsnmp;

/**
 * @package dautkom\zxpon
 */
abstract class Core
{

    /**
     * @var string
     */
    protected static $ip;

    /**
     * array( 'rack' => '', 'card' => '', 'olt'  => '', 'ont'  => '' )
     * @var array
     */
    protected static $port;

    /**
     * @var netsnmp\NetSNMP
     */
    protected static $snmp;

    /**
     * @var string
     */
    protected static $instance_olt;

    /**
     * @var string
     */
    protected static $instance_onu;

    /**
     * @var string
     */
    protected static $model;


    /**
     * Wrapper for snmpget(). Fetch an SNMP object. Accepts both strings and arrays as OIDs.
     * When $oid is an array and keys in results will be taken exactly as in object_id.
     *
     * @param  array|string $oid SNMP object identifier
     * @return mixed
     */
    protected function get($oid)
    {

        try {
            $snmp_get = self::$snmp->get($oid);
        }
        catch (\Throwable $t) {
            $snmp_get = false;
        }

        return $snmp_get;

    }


    /**
     * Wrapper for snmpwalk() and snmprealwalk(). Fetch SNMP object subtree
     *
     * @param  string $oid  SNMP object identifier representing root of subtree to be fetched
     * @param  bool   $real [optional] By default full OID notation is used for keys in output array. If set to <b>TRUE</b> subtree prefix will be removed from keys leaving only suffix of object_id.
     * @return array
     */
    protected function walk(string $oid, bool $real = false): array
    {

        try {
            $snmp_walk = self::$snmp->walk($oid, $real);
        }
        catch (\Throwable $t) {
            $snmp_walk = [];
        }

        return $snmp_walk;

    }

}
