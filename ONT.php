<?php

namespace dautkom\zxpon;


/**
 * @package dautkom\zxpon
 */
class ONT extends OLT
{

    /**
     * Device information from ini file.
     *
     * @var array
     */
    public static $device = [];

    /**
     * @ignore
     * @throws \Exception
     */
    public function __construct()
    {
        if( is_null(parent::$snmp) || is_null(parent::$snmp) ) {
            throw new \Exception('Impossible to instantiate ONT directly, connect to OLT first');
        }
    }


    /**
     * @param  array $coords
     * @param  bool  $debug
     * @return ONT
     * @throws \RuntimeException
     */
    public function connect($coords, $debug=false)
    {

        $coords = $this->filterCoords($coords);

        /**
         * Retrieve OLD instance ID. To accompish this task, first we want to determine port $card_number
         * sequencial number. The problem is, that OLT ports on second card start with OLT-1 (the same as
         * in the first card)
         */
        try {
            $query = @parent::$snmp->walk(".1.3.6.1.4.1.3902.1012.3.13.1.1.1", true);
            $i = 0;
            $card_number = ($coords[1] * 8) - (8 - $coords[2]);
            $instance = null;
        }
        catch (\Throwable $e) {
            throw new \RuntimeException("Unable to connect to OLT ".parent::$ip.". Check if device is offline or has misconfigured SNMP settings.");
        }

        foreach ($query as $key => $value) {
            $i++;
            if($i == $card_number){
                $instance = $key;
            }
        }

        if(!$instance){
            throw new \RuntimeException( "No information about OLT: ".parent::$port['olt'] );
        }
        parent::$instance_olt = $instance;

        // Retrieve all registered ONTs
        $query = parent::$snmp->walk(".1.3.6.1.4.1.3902.1012.3.28.1.1.1.$instance", true);

        // Find model
        foreach ($query as $key => $value) {
            if ($key == parent::$port['ont']) {
                parent::$model = preg_replace('/[\W]/', '', $value);
            }
        }

        if(!parent::$model) {
            throw new \RuntimeException( "No information about ONT ID: ".parent::$port['ont'] );
        }

        // Count ONU SNMP instance ID
        $startInstance  = 1073151744; // initial instance ID
        $gap['ont']     = 256;
        $gap['olt']     = 65536;
        $gap['card']    = 524288;

        parent::$instance_onu = $startInstance + (parent::$port['card'] * $gap['card']) + (parent::$port['olt'] * $gap['olt']) + (parent::$port['ont'] * $gap['ont']);
        $class                = "dautkom\\zxpon\\ont\\".parent::$model."\\Common";

        return new $class;

    }

    /**
     * @throws \ReflectionException
     */
    protected function loadIni()
    {
        $invoker = new \ReflectionClass($this);
        $invoker = $invoker->getNamespaceName();


        // Create path to ini file
        $ini = __DIR__ . str_replace([ __NAMESPACE__, '\\'], ['', DIRECTORY_SEPARATOR], $invoker) . DIRECTORY_SEPARATOR . 'model.ini';

        if (!file_exists( $ini )) {
            throw new \RuntimeException("$ini not found");
        }

        self::$device = $this->parseIni($ini);
    }

    /**
     * @param  array $coords
     * @return array
     * @throws \RuntimeException
     */
    private function filterCoords(array $coords): array
    {

        if( !is_array($coords) || count($coords) != 4 ) {
            throw new \RuntimeException("Wrong coordinates");
        }

        $coords       = array_map(function($val){ return intval($val); }, $coords);
        parent::$port = array_combine(parent::$port, $coords);

        return $coords;

    }

    /**
     * Parse and validate ini file
     *
     * @param   $ini string path to ini file
     * @throws \RuntimeException
     * @return  array
     */
    private function parseIni($ini): array
    {

        $ini = parse_ini_file( $ini, true );

        if (empty($ini)) {
            throw new \RuntimeException('Error while loading ' . parent::$model . ' ini-file, probably syntax error found');
        }

        if (!array_key_exists('ports', $ini) || empty($ini['ports'])) {
            throw new \RuntimeException('No ports defined in ' . parent::$model . ' ini-file');
        }

        return $ini;

    }

}
